﻿using AnyCompanyWebApiServer.Data.Models;
using AnyCompanyWebApiServer.Services;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using FromBodyAttribute = Microsoft.AspNetCore.Mvc.FromBodyAttribute;
using HttpDeleteAttribute = Microsoft.AspNetCore.Mvc.HttpDeleteAttribute;
using HttpPostAttribute = Microsoft.AspNetCore.Mvc.HttpPostAttribute;
using RouteAttribute = Microsoft.AspNetCore.Mvc.RouteAttribute;

namespace AnyCompanyWebApiServer.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class ContactController : ControllerBase
    {
        private ContactService _contactService;

        public ContactController(ContactService contactService)
        {
            _contactService = contactService;
        }

        /// <summary>
        /// Добавить контакт
        /// </summary>
        /// <param name="model"></param>
        /// <returns></returns>
        [HttpPost]
        [Route("createContact")]
        public async Task<IActionResult> CreateContact([FromBody] Contact model)
        {
            var result = await _contactService.CreateContact(model);
            return Ok(result);
        }

        /// <summary>
        /// Получить список контактов по фильтру
        /// </summary>
        /// <param name="model"></param>
        /// <returns></returns>
        [HttpPost]
        [Route("getContactsInfo")]
        public async Task<IActionResult> GetContactsInfo([FromBody] Contact model)
        {
            var result = await _contactService.GetContactsInfo(model);
            return Ok(result);
        }

        /// <summary>
        /// Редактировать инфо о контакте
        /// </summary>
        /// <param name="model"></param>
        /// <returns></returns>
        [HttpPost]
        [Route("updateContact")]
        public async Task<IActionResult> UpdateContact([FromBody] Contact model)
        {
            var result = await _contactService.UpdateContact(model);
            return Ok(result);
        }

        /// <summary>
        /// Удалить контакт
        /// </summary>
        /// <param name="id"></param>
        /// <returns></returns>
        [HttpDelete]
        [Route("deleteContact/{id}")]
        public async Task<IActionResult> DeleteContact(Guid id)
        {
            var result = await _contactService.DeleteContact(id);
            return Ok(result);
        }
    }
}
