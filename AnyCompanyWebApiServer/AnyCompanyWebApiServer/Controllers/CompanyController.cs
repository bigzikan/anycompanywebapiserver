﻿using AnyCompanyWebApiServer.Data.Models;
using AnyCompanyWebApiServer.Services;
using Microsoft.AspNetCore.Mvc;
using System.Web.Http;
using FromBodyAttribute = Microsoft.AspNetCore.Mvc.FromBodyAttribute;
using HttpDeleteAttribute = Microsoft.AspNetCore.Mvc.HttpDeleteAttribute;
using HttpPostAttribute = Microsoft.AspNetCore.Mvc.HttpPostAttribute;
using RouteAttribute = Microsoft.AspNetCore.Mvc.RouteAttribute;

namespace AnyCompanyWebApiServer.Controllers
{
    [RoutePrefix("api/[controller]")]
    [ApiController]
    public class CompanyController : ControllerBase
    {
        private CompanyService _companyService;

        public CompanyController(CompanyService companyService)
        {
            _companyService = companyService;
        }

        /// <summary>
        /// Добавить компанию
        /// </summary>
        /// <param name="model"></param>
        /// <returns></returns>
        [HttpPost]
        [Route("createCompany")]
        public async Task<IActionResult> CreateCompany([FromBody] Company model)
        {
            var result = await _companyService.CreateCompany(model);
            return Ok(result);
        }

        /// <summary>
        /// Получить список компаний по фильтру
        /// </summary>
        /// <param name="model"></param>
        /// <returns></returns>
        [HttpPost]
        [Route("getCompaniesInfo")]
        public async Task<IActionResult> GetCompaniesInfo([FromBody] Company model)
        {
            var result = await _companyService.GetCompaniesInfo(model);
            return Ok(result);
        }

        /// <summary>
        /// Редактировать инфо о компании
        /// </summary>
        /// <param name="model"></param>
        /// <returns></returns>
        [HttpPost]
        [Route("updateCompany")]
        public async Task<IActionResult> UpdateCompany([FromBody] Company model)
        {
            var result = await _companyService.UpdateCompany(model);
            return Ok(result);
        }

        /// <summary>
        /// Удалить компанию
        /// </summary>
        /// <param name="id"></param>
        /// <returns></returns>
        [HttpDelete]
        [Route("deleteCompany/{id}")]
        public async Task<IActionResult> DeleteCompany(Guid id)
        {
            var result = await _companyService.DeleteCompany(id);
            return Ok(result);
        }
    }
}
