﻿using AnyCompanyWebApiServer.Data.Models;
using AnyCompanyWebApiServer.Services;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;

namespace AnyCompanyWebApiServer.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class CommunicationController : ControllerBase
    {
        private CommunicationService _communicationService;

        public CommunicationController(CommunicationService communicationService)
        {
            _communicationService = communicationService;
        }

        /// <summary>
        /// Добавить коммуникацию
        /// </summary>
        /// <param name="model"></param>
        /// <returns></returns>
        [HttpPost]
        [Route("createСommunication")]
        public async Task<IActionResult> CreateСommunication([FromBody] Contact model)
        {
            var result = await _communicationService.CreateСommunication(model);
            return Ok(result);
        }

        /// <summary>
        /// Получить список коммуникаций по фильтру
        /// </summary>
        /// <param name="model"></param>
        /// <returns></returns>
        [HttpPost]
        [Route("getСommunicationsInfo")]
        public async Task<IActionResult> GetСommunicationsInfo([FromBody] Contact model)
        {
            var result = await _communicationService.GetСommunicationsInfo(model);
            return Ok(result);
        }

        /// <summary>
        /// Удалить коммуникацию
        /// </summary>
        /// <param name="id"></param>
        /// <returns></returns>
        [HttpDelete]
        [Route("deleteСommunication/{id}")]
        public async Task<IActionResult> DeleteСommunication(Guid id)
        {
            var result = await _communicationService.DeleteСommunication(id);
            return Ok(result);
        }
    }
}
