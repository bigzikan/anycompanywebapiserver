﻿using AnyCompanyWebApiServer.Data.Interfaces;
using AnyCompanyWebApiServer.Data.Models;

namespace AnyCompanyWebApiServer.Services
{
    public class CommunicationService
    {
        private ICommunication _communication;

        public CommunicationService(ICommunication communication)
        {
            _communication = communication;
        }

        /// <summary>
        /// Добавить коммуникацию
        /// </summary>
        /// <param name="model"></param>
        /// <returns></returns>
        public async Task<Guid> CreateСommunication(Contact model)
        {
            return await _communication.CreateСommunication(model);
        }

        /// <summary>
        /// Получить список коммуникаций по фильтру
        /// </summary>
        /// <param name="model"></param>
        /// <returns></returns>
        public async Task<List<Company>> GetСommunicationsInfo(Contact model)
        {
            return await _communication.GetСommunicationsInfo(model);
        }

        /// <summary>
        /// Удалить коммуникацию
        /// </summary>
        /// <param name="id"></param>
        /// <returns></returns>
        public async Task<bool> DeleteСommunication(Guid id)
        {
            return await _communication.DeleteСommunication(id);
        }
    }
}
