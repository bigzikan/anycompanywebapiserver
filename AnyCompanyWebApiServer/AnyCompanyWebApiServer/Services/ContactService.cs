﻿using AnyCompanyWebApiServer.Data.Interfaces;
using AnyCompanyWebApiServer.Data.Models;

namespace AnyCompanyWebApiServer.Services
{
    public class ContactService
    {
        private IContact _contact;

        public ContactService(IContact contact)
        {
            _contact = contact;
        }

        /// <summary>
        /// Добавить сотрудника
        /// </summary>
        /// <param name="model"></param>
        /// <returns></returns>
        public async Task<Guid> CreateContact(Contact model)
        {
            return await _contact.CreateContact(model);
        }

        /// <summary>
        /// Получить список сотрудников по фильтру
        /// </summary>
        /// <param name="model"></param>
        /// <returns></returns>
        public async Task<List<Company>> GetContactsInfo(Contact model)
        {
            return await _contact.GetContactsInfo(model);
        }

        /// <summary>
        /// Редактировать инфо о сотруднике
        /// </summary>
        /// <param name="model"></param>
        /// <returns></returns>
        public async Task<bool> UpdateContact(Contact model)
        {
            return await _contact.UpdateContact(model);
        }

        /// <summary>
        /// Удалить сотрудника
        /// </summary>
        /// <param name="id"></param>
        /// <returns></returns>
        public async Task<bool> DeleteContact(Guid id)
        {
            return await _contact.DeleteContact(id);
        }
    }
}
