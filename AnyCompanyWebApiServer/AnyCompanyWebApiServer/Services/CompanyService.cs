﻿using AnyCompanyWebApiServer.Data.Interfaces;
using AnyCompanyWebApiServer.Data.Models;

namespace AnyCompanyWebApiServer.Services
{
    public class CompanyService
    {
        private readonly ICompany _company;

        public CompanyService(ICompany company)
        {
            _company = company;
        }

        /// <summary>
        /// Добавить компанию
        /// </summary>
        /// <param name="model"></param>
        /// <returns></returns>
        public async Task<Guid> CreateCompany(Company model)
        {
            return await _company.CreateCompany(model);
        }

        /// <summary>
        /// Получить список компаний по фильтру
        /// </summary>
        /// <param name="model"></param>
        /// <returns></returns>
        public async Task<List<Company>> GetCompaniesInfo(Company model)
        {
            return await _company.GetCompaniesInfo(model);
        }

        /// <summary>
        /// Редактировать инфо о компании
        /// </summary>
        /// <param name="model"></param>
        /// <returns></returns>
        public async Task<bool> UpdateCompany(Company model)
        {
            return await _company.UpdateCompany(model);
        }

        /// <summary>
        /// Удалить компанию
        /// </summary>
        /// <param name="id"></param>
        /// <returns></returns>
        public async Task<bool> DeleteCompany(Guid id)
        {
            return await _company.DeleteCompany(id);
        }
    }
}
