﻿using AnyCompanyWebApiServer.Enums;

namespace AnyCompanyWebApiServer.Data.Models
{
    public class Сommunication
    {
        /// <summary>
        /// Id
        /// </summary>
        public Guid Id { get; set; }

        /// <summary>
        /// Id компании
        /// </summary>
        public Guid CompanyId { get; set; }

        /// <summary>
        /// Сотрудник
        /// </summary>
        public Guid ContactId { get; set; }

        /// <summary>
        /// Тип связи
        /// </summary>
        public TypeEnum Type { get; set; }

        /// <summary>
        /// Телефон
        /// </summary>
        public string? PhoneNumber { get; set; }

        /// <summary>
        /// Email
        /// </summary>
        public string? Email { get; set; }
    }
}
