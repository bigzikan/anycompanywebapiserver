﻿using AnyCompanyWebApiServer.Data.Models;

namespace AnyCompanyWebApiServer.Data.Interfaces
{
    public interface ICommunication
    {
        /// <summary>
        /// Добавить коммуникацию
        /// </summary>
        /// <param name="model"></param>
        /// <returns></returns>
        Task<Guid> CreateСommunication(Contact model);

        /// <summary>
        /// Получить список коммуникаций по фильтру
        /// </summary>
        /// <param name="model"></param>
        /// <returns></returns>
        Task<List<Company>> GetСommunicationsInfo(Contact model);

        /// <summary>
        /// Удалить коммуникацию
        /// </summary>
        /// <param name="id"></param>
        /// <returns></returns>
        Task<bool> DeleteСommunication(Guid id);
    }
}
