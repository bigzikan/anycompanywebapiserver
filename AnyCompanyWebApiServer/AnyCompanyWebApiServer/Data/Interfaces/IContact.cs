﻿using AnyCompanyWebApiServer.Data.Models;

namespace AnyCompanyWebApiServer.Data.Interfaces
{
    public interface IContact
    {
        /// <summary>
        /// Добавить сотрудника
        /// </summary>
        /// <param name="model"></param>
        /// <returns></returns>
        Task<Guid> CreateContact(Contact model);

        /// <summary>
        /// Получить список сотрудников по фильтру
        /// </summary>
        /// <param name="model"></param>
        /// <returns></returns>
        Task<List<Company>> GetContactsInfo(Contact model);

        /// <summary>
        /// Редактировать инфо о сотруднике
        /// </summary>
        /// <param name="model"></param>
        /// <returns></returns>
        Task<bool> UpdateContact(Contact model);

        /// <summary>
        /// Удалить сотрудника
        /// </summary>
        /// <param name="id"></param>
        /// <returns></returns>
        Task<bool> DeleteContact(Guid id);
    }
}
