﻿using AnyCompanyWebApiServer.Data.Models;
using Microsoft.EntityFrameworkCore;
using System.Transactions;

namespace AnyCompanyWebApiServer.Data
{
    public class ServerDBContext : DbContext
    {
        public ServerDBContext(DbContextOptions<ServerDBContext> options) : base(options)
        { 
        }

        public DbSet<Company> Companies { get; set; }
        public DbSet<Contact> Contacts { get; set; }
        public DbSet<Сommunication> Сommunications { get; set; }
    }
}
