﻿using AnyCompanyWebApiServer.Data.Interfaces;
using AnyCompanyWebApiServer.Data.Models;

namespace AnyCompanyWebApiServer.Data.Repository
{
    public class CompanyRepository : ICompany
    {
        public Task<Guid> CreateCompany(Company model)
        {
            throw new NotImplementedException();
        }

        public Task<bool> DeleteCompany(Guid id)
        {
            throw new NotImplementedException();
        }

        public Task<List<Company>> GetCompaniesInfo(Company model)
        {
            throw new NotImplementedException();
        }

        public Task<bool> UpdateCompany(Company model)
        {
            throw new NotImplementedException();
        }
    }
}
