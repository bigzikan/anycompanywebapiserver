﻿using AnyCompanyWebApiServer.Data.Interfaces;
using AnyCompanyWebApiServer.Data.Models;

namespace AnyCompanyWebApiServer.Data.Repository
{
    public class ContactRepository : IContact
    {
        Task<Guid> IContact.CreateContact(Contact model)
        {
            throw new NotImplementedException();
        }

        Task<bool> IContact.DeleteContact(Guid id)
        {
            throw new NotImplementedException();
        }

        Task<List<Company>> IContact.GetContactsInfo(Contact model)
        {
            throw new NotImplementedException();
        }

        Task<bool> IContact.UpdateContact(Contact model)
        {
            throw new NotImplementedException();
        }
    }
}
